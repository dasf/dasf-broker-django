.. _configuration:

Configuration options
=====================

Configuration settings
----------------------

The following settings have an effect on the app.

.. automodulesumm:: dasf_broker.app_settings
    :autosummary-no-titles:
