"""A simple backend module for DASF."""

from demessaging import main

__all__ = ["hello_world"]


def hello_world() -> str:
    """Simple function to be called via DASF"""
    return "Hello World!"


if __name__ == "__main__":
    main()
