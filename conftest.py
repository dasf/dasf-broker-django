"""pytest configuration script for dasf-broker-django."""

# Disclaimer
# ----------
#
# Copyright (C) 2022 Helmholtz-Zentrum Hereon
#
# This file is part of the project dasf-broker-django
# and is released under the EUPL-1.2 license. See LICENSE in the root of the
# repository for full licensing details.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 or later
# as published by the European Commission.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# EUPL-1.2 license for more details.
#
# You should have received a copy of the EUPL-1.2 license along with this
# program. If not, see https://www.eupl.eu/.

from pathlib import Path
from typing import Callable

import pytest


@pytest.fixture(scope="session")
def base_test_modules_path() -> Path:
    return Path(__file__).parent / "tests" / "test_modules"


@pytest.fixture(scope="session")
def get_test_module_path(
    base_test_modules_path: Path,
) -> Callable[[str], str]:
    """Get the path to a test module"""

    def get_path(mod: str) -> str:
        return str(base_test_modules_path / (mod + ".py"))

    return get_path
