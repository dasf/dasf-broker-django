from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
    re_path(r"^status-ping/?$", consumers.PongConsumer.as_asgi()),
    re_path(
        r"^(?P<slug>[-_\w]+)/?$",
        consumers.TopicProducer.as_asgi(),
    ),
    re_path(
        r"^(?P<slug>[-_\w]+)/(?P<subscription>[-\.:\w]+)/?$",
        consumers.TopicConsumer.as_asgi(),
    ),
]
