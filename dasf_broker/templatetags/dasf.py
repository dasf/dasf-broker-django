"""DASF Template tags
------------------

This module contains the template tags that should be used within DASF.
"""


# Disclaimer
# ----------
#
# Copyright (C) 2022 Helmholtz-Zentrum Hereon
#
# This file is part of dasf-broker-django and is released under the
# EUPL-1.2 license.
# See LICENSE in the root of the repository for full licensing details.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 or later
# as published by the European Commission.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# EUPL-1.2 license for more details.
#
# You should have received a copy of the EUPL-1.2 license along with this
# program. If not, see https://www.eupl.eu/.


from __future__ import annotations

import base64
import json
from typing import TYPE_CHECKING

from django import template

from dasf_broker import models

if TYPE_CHECKING:
    from dasf_broker.models import BrokerMessage

register = template.Library()


@register.simple_tag(takes_context=True)
def dasf_ws_url(context, route: str) -> str:
    """Get the url route to a websocket.

    Parameters
    ----------
    route: str
        The location of the route

    Examples
    --------
    Call this tag in your template like::
        {% load dasf %}
        {% dasf_ws_url "test-topic" %}

    which will resolve to something like `ws://localhost:8000/ws/test-topic`
    (depending on your
    :attr:`~dasf_broker.app_settings.DASF_WEBSOCKET_URL_ROUTE`)
    """
    return models.BrokerTopic.build_websocket_url(context["request"], route)


@register.filter
def payload(message: BrokerMessage):
    payload = base64.b64decode(message.content.get("payload", "")).decode(
        "utf-8"
    )
    try:
        return json.loads(payload)
    except json.JSONDecodeError:
        return None


@register.inclusion_tag("dasf_broker/payload_str.html")
def payload_pre(message: BrokerMessage):
    """The payload as json formatted string"""
    payload = base64.b64decode(message.content.get("payload", "")).decode(
        "utf-8"
    )
    try:
        loaded = json.loads(payload)
    except json.JSONDecodeError:
        return {"payload": payload}
    else:
        return {"payload": json.dumps(loaded, indent=4)}
